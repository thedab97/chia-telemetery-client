@echo off


git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git C:\ecopool-client
curl https://nodejs.org/dist/v12.13.0/node-v12.13.0-x64.msi --output C:\node-v12.13.0-x64.msi

C:\node-v12.13.0-x64.msi

cd C:\ecopool-client

SET DEBUG=EcoPool:Client*
SET NODE_ENV=production

CMD /C npm i -g pm2
CMD /C pm2 delete all

CMD /C npm i -g yarn

CMD /C yarn install

CMD /C pm2 start "C:\Program Files\nodejs\node.exe" --name api -- ./backend/bin/www
CMD /C pm2 start .\node_modules\@vue\cli-service\bin\vue-cli-service.js --name frontend -- serve 
CMD /C pm2 save


chcp 1251 >NUL

set rus_info= "������ ecopool ����� ������� �� ������ http://localhost:3401/ � http://$(hostname -I):3401 ����� ��������� ���������"

chcp 866 >NUL

echo ""
echo ""
echo ""
echo ""
echo "================================================================================="
echo "Ecopool client will be available on url http://localhost:3401/ and http://$(hostname -I):3401 in several moments"
echo %rus_info%
echo "================================================================================="
echo "Press any key to continue"
pause > nul