import byteSize from "byte-size";

export default {
    methods: {
        getIecValue(size) {
            const bs = byteSize(size, {
                units: 'iec',
                precision: 4
            });
            bs.toString = function () {
                return `${this.value} ${this.unit}`;
            }

            return bs;
        }
    }
}