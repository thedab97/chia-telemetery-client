import packageSrc from '../../../package.json';

const state = () => ({
    pathStatus: false,
    isAuth: false,
    newVersion: packageSrc.version,
    userBalance: 0,
    pendingWithdrawals: []
});

const getters = {
        isAuth: state => state.isAuth,
        isPathStatus: state => state.pathStatus,
    };

const mutations = {
    setPathStatus: (state, status) => {
        state.pathStatus = status;
    },
    setAuth:(state, newVal) => {
        state.isAuth = newVal;
    },
    setNewVersion: (state, newVal) => {
        state.newVersion = newVal;
    },
    setBalance: (state, newVal) => {
        state.userBalance = newVal;
    },
    setPendingWithdrawals: (state, newVal) => {
        state.pendingWithdrawals = newVal;
    },
    logout: (state) => {
        state.isAuth = false;
        state.pathStatus = false;
    }
};

const actions = {};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}
