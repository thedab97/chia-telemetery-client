import { io } from 'socket.io-client';
const socket = io(`${window.location.protocol}//${window.location.hostname}:4400`, {
    reconnectionDelay: 1000,
});

socket.on('connect', () => {
    console.log('Sockets successfully connected');
});

export default socket;
