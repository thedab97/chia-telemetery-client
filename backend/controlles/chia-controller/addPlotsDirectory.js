const { Harvester } = require('chia-client');

const HarvesterInstance = new Harvester();

module.exports = async (directory) => {
    return await HarvesterInstance.addPlotDirectory(directory);
};
