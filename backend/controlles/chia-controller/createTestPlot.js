const POOL_KEYS = require('../../constants/PoolKeys');
const callCLI = require('./callCLI');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('chia-controller:create-test-plot');

module.exports = (tempFolder, persistentFolder) => {
    if (!tempFolder || !persistentFolder) {
        debugError('Не передана временная или постоянная папка');
        return false;
    }

    debugLog('Создание тестового плота начато');

    return callCLI({
        'plots': null,
        'create': null,
        '-k': 25,
        '--override-k': null,
        '-r': 2, // количество потоков
        '-b': 512, // RAM
        '-n': 1, // количество участков подряд
        '-u': 128, // количество корзин
        '-t': tempFolder, // временная директория
        '-d': persistentFolder, // финальная директория
        '-f': POOL_KEYS.farmer,
        '-p': POOL_KEYS.pool
    });
};
