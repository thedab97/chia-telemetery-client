const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'stop': null,
        'all': null,
        '-d': null
    });

    return checkCommand.status;
};
