const config = require('config');
const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'configure': null,
        '--set-farmer-peer': null,
        [`${config.pool.ip}:8447`]: null
    });

    return checkCommand.status;
};
