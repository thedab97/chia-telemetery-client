const discoverCLI = require('./discoverCLI');
const callCLI = require('./callCLI');
const callCLIPromise = require('./callCLIPromise');
const checkCLI = require('./checkCLI');
const stopAllProcess = require('./stopAllProcess');
const setCertificates = require('./setCertificates');
const setUpnp = require('./setUpnp');
const setFarmerPeer = require('./setFarmerPeer');
const setLogToDebug = require('./setLogToDebug');
const startHarvester = require('./startHarvester');
const logWatcher = require('./logWatcher');
const createTestPlot = require('./createTestPlot');
const createPlot = require('./createPlot');
const getPlotMemo = require('./getPlotMemo');
const getPlots = require('./getPlots');
const getBadPlots = require('./getBadPlots');
const refreshPlots = require('./refreshPlots');
const addPlotsDirectory = require('./addPlotsDirectory');
const checkPlots = require('./checkPlots');
const init = require('./init');

module.exports = {
    discoverCLI,
    callCLI,
    callCLIPromise,
    checkCLI,
    stopAllProcess,
    startHarvester,
    logWatcher,
    setCertificates,
    setUpnp,
    setFarmerPeer,
    setLogToDebug,
    createTestPlot,
    createPlot,
    getPlotMemo,
    getPlots,
    getBadPlots,
    refreshPlots,
    addPlotsDirectory,
    checkPlots,
    init
};
