const PLOTS_SIZES = require('../../constants/PlotsSizes');
const DEFAULT_PLOTS_RAM_SIZES = require('../../constants/DefaultPlotsRamSizes');
const POOL_KEYS = require('../../constants/PoolKeys');
const callCLI = require('./callCLI');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:create-plot');

module.exports = (payload) => {
    let {
        size,
        ram,
        threads,
        buckets,
        bitField = false,
        tempFolder,
        tempFolder2,
        persistentFolder
    } = payload;

    if (!tempFolder) {
        debugError('Не передана временная папка');
        return false;
    }

    if (!persistentFolder) {
        debugError('Не передана постоянная папка');
        return false;
    }

    if (!PLOTS_SIZES.includes(size)) {
        debugWarn('Размер плота передан некорректный, по дефолту установлен размер k32');
        size = 32;
    }

    if (!ram) {
        debugWarn(`Не передан размер оперативной памяти, по дефолту задан ${DEFAULT_PLOTS_RAM_SIZES[32]} MiB`);
        ram = DEFAULT_PLOTS_RAM_SIZES[32];
    }

    if (!threads) {
        debugWarn('Не передано количество потоков, по дефолту установлено 2');
        threads = 2;
    }

    if (!buckets) {
        debugWarn('Не передано количество корзин, по дофолту установлено 128');
        buckets = 128;
    }

    const plotParams = {
        'plots': null,
        'create': null,
        '-k': size,
        '-b': ram,
        '-r': threads,
        '-u': buckets,
        '-n': 1,
        '-t': tempFolder,
        '-d': persistentFolder,
        '-f': POOL_KEYS.farmer,
        '-p': POOL_KEYS.pool
    };

    if (bitField) {
        plotParams['-e'] = null;
    }

    if (tempFolder2) {
        plotParams['-2'] = tempFolder2;
    }

    const stringPlotParams = `size: k${size}, ram: ${ram}, threads: ${threads}, buckets: ${buckets}, bitField: ${bitField}, tempFolder: ${tempFolder}, ${tempFolder2 ? `tempFolder2: ${tempFolder2}, ` : ''} persistentFolder: ${persistentFolder}`;

    debugLog(`Создание плота с параметрами: (${stringPlotParams}) начато`);

    return callCLI(plotParams);
};
