const getCheckPlotsDataFromLog = require('../../../utils/getCheckPlotsDataFromLog');
const getLogs = require('../../../utils/getLogs');
const { debugLog } = getLogs('chia-controller:log-watcher:check-plots');
const state = require('../../../state');

let lastTime = null;

module.exports = (lastLogString, sockets) => {
    const plotsData = getCheckPlotsDataFromLog(lastLogString);

    if (!plotsData) {
        return false;
    }

    const time = new Date().getTime();

    if (time === lastTime) {
        return false;
    }

    state.checkPlotsHistory.unshift({
        time: plotsData.time,
        timestamp: time
    });
    state.checkPlotsHistory = state.checkPlotsHistory.slice(0, 200);
    lastTime = time;

    debugLog('Data about plots checking sent to client');
    sockets.emit('check-plots', state.checkPlotsHistory);
};
