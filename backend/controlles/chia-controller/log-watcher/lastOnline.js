const getHarvesterConnectionTimeFromLog = require('../../../utils/getHarvesterConnectionTimeFromLog');
const getLogs = require('../../../utils/getLogs');
const { debugLog } = getLogs('chia-controller:log-watcher:last-online');

let oldDateOnline = null;

module.exports = (lastLogString, sockets) => {
    const date = getHarvesterConnectionTimeFromLog(lastLogString);

    if (!date) {
        return false;
    }

    const lastDateOnline = new Date(date).getTime();
    let timeDiff = 0;

    if (oldDateOnline) {
        timeDiff = (lastDateOnline - oldDateOnline) / 1000;
    }

    oldDateOnline = lastDateOnline;

    if (timeDiff < 1) {
        return false;
    }

    const connection = timeDiff < 20
        ? 'perfect'
        : timeDiff >= 20 && timeDiff < 60
            ? 'good'
            : 'bad'
    ;
    const connectionData = {
        ping: timeDiff,
        connection
    };

    debugLog(`Data on connection sent to client (ping: ${timeDiff}, connection: ${connection})`);
    sockets.emit('harvester-connection', connectionData);
};
