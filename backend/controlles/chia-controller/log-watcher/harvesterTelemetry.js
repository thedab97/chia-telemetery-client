const EcoPoolController = require('../../ecopool-controller');
const getCheckPlotsDataFromLog = require('../../../utils/getCheckPlotsDataFromLog');
const getFarmingPlotsDataFromLog = require('../../../utils/getFarmingPlotsDataFromLog');
const getHarvesterConnectionTimeFromLog = require('../../../utils/getHarvesterConnectionTimeFromLog');
const getBytesFromTibs = require('../../../utils/getBytesFromTibs');
const getLogs = require('../../../utils/getLogs');
const { debugLog } = getLogs('chia-controller:log-watcher:harvester-telemetry');
const state = require('../../../state');

const telemetryData = {
    totalPlots: null,
    totalPlotsSize: null,
    eligiblePlots: null,
    proofs: null,
    checkPlotsTime: null,
    eligiblePlotsTime: null,
    challengeResponseTime: null
};

let lastFarmingTime = null;
let lastChallengeResponseTime = null;

module.exports = (lastLogString, sockets) => {
    const totalPlotsData = getCheckPlotsDataFromLog(lastLogString);
    const farmingPlotsData = getFarmingPlotsDataFromLog(lastLogString);
    const connectionTime = getHarvesterConnectionTimeFromLog(lastLogString);

    if (totalPlotsData) {
        telemetryData.totalPlots = totalPlotsData.count;
        telemetryData.totalPlotsSize = getBytesFromTibs(totalPlotsData.size);
        telemetryData.checkPlotsTime = totalPlotsData.time;
    }

    if (farmingPlotsData) {
        telemetryData.eligiblePlots = farmingPlotsData.count;
        telemetryData.proofs = farmingPlotsData.proofs;
        telemetryData.eligiblePlotsTime = farmingPlotsData.time;

        const farmingTime = new Date(farmingPlotsData.date).getTime();

        if (lastFarmingTime !== farmingTime) {
            state.farmingPlotsHistory.unshift({
                timestamp: farmingTime,
                passedFilter: farmingPlotsData.count,
                proofs: farmingPlotsData.proofs,
                total: farmingPlotsData.totalPlots
            });
            state.farmingPlotsHistory = state.farmingPlotsHistory.slice(0, 10);
            state.proofsHistory.unshift({
                proofs: farmingPlotsData.proofs,
                timestamp: new Date().getTime()
            });
            state.proofsHistory = state.proofsHistory.slice(0, 50);

            debugLog('Data about eligible plots and proofs sent to client');
            sockets.emit('harvester-telemetry', {
                filter: state.farmingPlotsHistory,
                proofs: state.proofsHistory
            });
        }

        lastFarmingTime = farmingTime;
    }

    let $challengeResponseTime;

    if (connectionTime) {
        telemetryData.challengeResponseTime = connectionTime;
        $challengeResponseTime = new Date(connectionTime).getTime();
    }

    const {
        totalPlots,
        totalPlotsSize,
        eligiblePlots,
        proofs,
        checkPlotsTime,
        eligiblePlotsTime,
        challengeResponseTime
    } = telemetryData;

    if (
        totalPlots &&
        totalPlotsSize &&
        eligiblePlots &&
        proofs &&
        checkPlotsTime &&
        eligiblePlotsTime &&
        challengeResponseTime &&
        (lastChallengeResponseTime !== $challengeResponseTime && $challengeResponseTime)
    ) {
        EcoPoolController.socketIoClient.emit('harvesterData', telemetryData);

        debugLog('Harvester data transferred to server');

        telemetryData.eligiblePlots = null;
        telemetryData.proofs = null;
        telemetryData.eligiblePlotsTime = null;
        telemetryData.challengeResponseTime = null;
        lastChallengeResponseTime = $challengeResponseTime;
    }
};
