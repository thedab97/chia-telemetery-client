const path = require('path');
const { homedir } = require('os');
const Tail = require('tail').Tail;
const pathToLogFile = path.join(homedir(), '.chia', 'mainnet', 'log', 'debug.log');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:log-watcher');

const lastOnline = require('./log-watcher/lastOnline');
const checkPlots = require('./log-watcher/checkPlots');
const harvesterTelemetry = require('./log-watcher/harvesterTelemetry');

let starting = false;
let TailInstance;

module.exports = {
    start (sockets) {
        if (!starting) {
            debugWarn('Chia logs checking already started, can only restart');
        }

        if (sockets) {
            debugLog('Sockets transferred to debug watcher');
        } else {
            debugWarn('Sockets hasnt transferred to debug watcher, data will not send!!!');
        }

        TailInstance = new Tail(pathToLogFile);

        TailInstance.on('line', data => {
            lastOnline(data, sockets);
            checkPlots(data, sockets);
            harvesterTelemetry(data, sockets);
        });

        TailInstance.on('error', error => {
            debugError(JSON.stringify({
                message: 'Error while reading chia logs',
                error
            }));
        });

        starting = true;
        debugLog('Chia logs watching already started');
    },
    restart () {
        TailInstance.unwatch();
        TailInstance.watch();
        debugLog('Chia logs watching restarted');
    },
    stop () {
        TailInstance.unwatch();
        starting = false;
        debugLog('Chia logs watching stopped');
    }
};
