const getLogs = require('../../utils/getLogs');
const checkCLI = require('./checkCLI');
const stopAllProcess = require('./stopAllProcess');
const setCertificates = require('./setCertificates');
const setUpnp = require('./setUpnp');
const setFarmerPeer = require('./setFarmerPeer');
const setLogToDebug = require('./setLogToDebug');
const startHarvester = require('./startHarvester');
const EcoPoolController = require('../ecopool-controller');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:init');
const state = require('../../state');

module.exports = async () => {
    if (state.harvesterIsWorking) {
        debugWarn('Харвестер уже запущен');
        EcoPoolController.plotsUploader.start();
        return false;
    }

    const checkCLIStatus = await checkCLI();

    if (!checkCLIStatus) {
        debugError('Путь до chia-blockchain не найден');
        return false;
    }

    debugLog('Путь до chia-blockchain успешно найден');

    const stopAllProcessStatus = await stopAllProcess();

    if (!stopAllProcessStatus) {
        debugError('Не удалось остановить все процессы chia');
    } else {
        state.harvesterIsWorking = false;
        debugLog('Все процессы chia успешно остановлены');
    }

    const setCertificatesStatus = await setCertificates();

    if (!setCertificatesStatus) {
        debugError('Не удалось установить сертификаты пула');
        return false;
    }

    debugLog('Сертификаты пула успешно установлены');

    const setUpnpnStatus = await setUpnp();

    if (!setUpnpnStatus) {
        debugError('Не удалось выставить upnp = false');
        return false;
    }

    debugLog('Успешно выставлено upnp = false');

    const setFarmerPeerStatus = await setFarmerPeer();

    if (!setFarmerPeerStatus) {
        debugError('Не удалось установить ip пула');
        return false;
    }

    debugLog('Успешно установлен ip пула');

    const setLogToDebugStatus = await setLogToDebug();

    if (!setLogToDebugStatus) {
        debugError('Не удалось установить логи в режиме DEBUG');
        return false;
    }

    debugLog('Логи успешно установлены в режиме DEBUG');

    const startHarvesterStatus = await startHarvester();

    if (!startHarvesterStatus) {
        debugError('Не удалось запустить харвестр');
        return false;
    }

    state.harvesterIsWorking = true;
    debugLog('Харвестер успешно запущен');

    setTimeout(() => {
        EcoPoolController.plotsUploader.start();
    }, 5000);

    return true;
};
