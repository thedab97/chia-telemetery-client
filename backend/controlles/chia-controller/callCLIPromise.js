const callCLI = require('./callCLI');

module.exports = (payload, customCLIPath, handler) => {
    return new Promise(resolve => {
        const child = callCLI(payload, customCLIPath, handler);

        child.on('exit', function (exitCode) {
            resolve({
                exitCode,
                status: exitCode === 0
            });
        });
    });
};
