const { Harvester } = require('chia-client');
const getPlotMemo = require('./getPlotMemo');
const getPreparedPlotDataForServer = require('../../utils/getPreparedPlotDataForServer');
const HarvesterInstance = new Harvester();
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('chia-controller:get-plots');

module.exports = async () => {
    await HarvesterInstance.refreshPlots();
    const plots = await HarvesterInstance.getPlots();

    if (!plots.success) {
        debugError('Метод getPlots у HarvesterInstance вернул success = false');
        return [];
    }

    const plotsData = plots.plots;

    if (!plotsData.length) {
        debugLog('У пользователя нет валидных плотов');
        return [];
    }

    const preparedPlotsData = [];

    for (const plotData of plotsData) {
        preparedPlotsData.push({
            ...getPreparedPlotDataForServer(plotData),
            memo: await getPlotMemo(plotData.filename)
        });
    }

    debugLog('Данные по валидным плотам успешно собраны');
    return preparedPlotsData;
};
