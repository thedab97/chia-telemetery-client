const { Harvester } = require('chia-client');
const HarvesterInstance = new Harvester();
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('chia-controller:get-bad-plots');

module.exports = async () => {
    await HarvesterInstance.refreshPlots();
    const plots = await HarvesterInstance.getPlots();

    if (!plots.success) {
        debugError('Метод getPlots у HarvesterInstance вернул success = false');
        return [];
    }

    const badPlots = plots.not_found_filenames;

    if (!badPlots.length) {
        debugLog('У пользователя нет невалидных плотов');
        return [];
    }

    debugLog('Данные по невалидным плотам успешно собраны');

    return badPlots;
};
