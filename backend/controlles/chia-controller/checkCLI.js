const discoverCLI = require('./discoverCLI');
const callCLIPromise = require('./callCLIPromise');

module.exports = async (customPath = null) => {
    const CLIPath = customPath ? customPath : discoverCLI();
    const checkCLICommand = await callCLIPromise({
        '-h': null
    }, CLIPath);

    return checkCLICommand.status;
};
