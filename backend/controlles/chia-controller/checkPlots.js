const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    let checkPlotsTime = 0;
    await callCLIPromise({
        'plots': null,
        'check': null
    }, null, (process, string) => {
        const needStringMatch = string.match(/[\d.]{3,} seconds/gm);

        if (needStringMatch) {
            checkPlotsTime = needStringMatch[0].match(/[\d.]{3,}/gm)[0];
            process.kill();
        }
    });

    return checkPlotsTime;
};
