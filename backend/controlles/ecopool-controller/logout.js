const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const GuiController = require('../../controlles/gui-controller');
const stop = require('./stop');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:logout');

module.exports = async () => {
    let requestData;

    try {
        requestData = await apiRequest({
            method: 'get',
            url: ECO_POOL_API.logout
        });
    } catch (error) {
        debugError(JSON.stringify({
            'message': 'При разлогине произошла ошибка',
            error
        }));
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;

    if (status !== 200) {
        debugError(`Код ответа сервера: ${status}`);
        return { success: false };
    }

    if (!success) {
        debugLog(JSON.stringify({
            message: 'Не удалось разлогиниться',
            reason: requestData.data.answer
        }));
    }

    GuiController.stop();
    stop();

    debugLog(JSON.stringify({
        message: 'Разлогин пользователя выполнен успешно',
        reason: requestData.data.answer
    }));

    return { success: true };
};
