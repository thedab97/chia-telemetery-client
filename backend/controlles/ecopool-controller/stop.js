const plotsUploader = require('./plotsUploader');
const socketIoClient = require('./socketIoClient');
const getLogs = require('../../utils/getLogs');
const { debugLog } = getLogs('ecopool-controller:stop');

module.exports = () => {
    socketIoClient.emit('dropUser');
    plotsUploader.stop();
    debugLog('Запущенные процессы ecopool controller-а успешно остановлены');
};
