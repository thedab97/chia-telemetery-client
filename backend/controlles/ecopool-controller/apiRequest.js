const axios = require('axios');
const config = require('config');
const state = require('../../state');
const getLogs = require('../../utils/getLogs');
const { debugLog } = getLogs('ecopool-controller:api-request');

module.exports = (payload) => {
    const {
        url,
        method = 'post',
        data = {}
    } = payload;
    const hasToken = Boolean(state.token);
    const preparedUrl = `${config.pool.host}${url}`;

    debugLog(`Выполняется запрос на ${preparedUrl}`);

    return axios({
        method,
        url: preparedUrl,
        data,
        headers: hasToken ? {
            Cookie: `connect.sid=${state.token}`
        } : {}
    })
        .finally(() => {
            debugLog(`Выполнен запрос на ${preparedUrl}`);
        });
};
