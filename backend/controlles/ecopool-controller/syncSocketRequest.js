const socketIoClient = require('./socketIoClient');

module.exports = async (queueName) => {
    return await new Promise(resolve => {
        socketIoClient.emit(queueName, resolve);
    });
};
