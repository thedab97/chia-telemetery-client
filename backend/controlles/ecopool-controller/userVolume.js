const syncSocketRequest = require('./syncSocketRequest');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:user-volume');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('my-volume');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Не удалось получить данные по мощности пользователя с сервера',
            error
        }));
        return { success: false };
    }

    debugLog('Данные по мощности пользователя получены с сервера');

    return {
        success: true,
        answer: requestData
    };
};
