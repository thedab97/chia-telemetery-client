const syncSocketRequest = require('./syncSocketRequest');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:user-balance');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('my-balance');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Не удалось получить баланс пользователя с сервера',
            error
        }));
        return { success: false };
    }

    debugLog('Баланс пользователя получен с сервера');

    return {
        success: true,
        answer: requestData
    };
};
