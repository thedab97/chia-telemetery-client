const apiRequest = require('./apiRequest');
const socketIoClient = require('./socketIoClient');
const syncSocketRequest = require('./syncSocketRequest');
const userRegistration = require('./registration');
const userLogin = require('./login');
const userLogout = require('./logout');
const addPlots = require('./addPlots');
const plotsUploader = require('./plotsUploader');
const poolVolume = require('./poolVolume');
const userBalance = require('./userBalance');
const userVolume = require('./userVolume');
const makeWithdraw = require('./makeWithdraw');
const pendingWithdrawals = require('./pendingWithdrawals');
const stop = require('./stop');

module.exports = {
    apiRequest,
    socketIoClient,
    syncSocketRequest,
    userRegistration,
    userLogin,
    userLogout,
    addPlots,
    plotsUploader,
    poolVolume,
    userVolume,
    userBalance,
    makeWithdraw,
    pendingWithdrawals,
    stop
};
