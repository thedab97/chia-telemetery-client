const syncSocketRequest = require('./syncSocketRequest');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:pool-volume');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('pool-volume');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Не удалось получить данные по мощности пула с сервера',
            error
        }));
        return { success: false };
    }

    debugLog('Данные по мощности пула получены с сервера');

    return {
        success: true,
        answer: requestData
    };
};
