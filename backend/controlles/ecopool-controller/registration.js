const setCookie = require('set-cookie-parser');
const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const GuiController = require('../gui-controller');
const socketIoClient = require('./socketIoClient');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:registration');
const state = require('../../state');

module.exports = async (payload) => {
    const { login } = payload;
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.registration,
            data: payload
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'При регистрации произошла ошибка',
            error
        }));
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;
    const parseCookies = setCookie.parse(requestData.headers['set-cookie']);
    const cookieSid = parseCookies.find(cookie => {
        return cookie.name === 'connect.sid';
    });
    const token = cookieSid && cookieSid.value || null;

    if (status !== 200) {
        debugError(`Код ответа сервера: ${status}`);
        return { success: false };
    }

    if (!success) {
        debugLog(JSON.stringify({
            message: 'Отказано в регистрации',
            reason: requestData.data.answer
        }));
        return {
            success: false,
            answer: requestData.data.answer
        };
    }

    debugLog('Регистрация прошла успешно');

    GuiController.localConfig.set('currentUser', login);
    socketIoClient.emit('setupUser', login, ({ success }) => {
        // TODO сделать комментарий более понятливым
        debugLog('Установлен пользователь', success);
    });

    if (token) {
        state.token = token;
    }

    return { success: true };
};
