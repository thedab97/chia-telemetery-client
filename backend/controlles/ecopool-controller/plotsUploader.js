const getPlots = require('../chia-controller/getPlots');
const addPlots = require('./addPlots');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:plots-uploader');

const sendPlots = async () => {
    const allPlots = await getPlots();
    addPlots(allPlots);
};

let isStarted = false;
let timeout = 1000 * 60 * 5; // 5 минут
let timer = null;

module.exports = {
    start: () => {
        if (isStarted) {
            debugError('Обновление плотов уже запущено');
            return false;
        }

        sendPlots();
        timer = setInterval(() => {
            sendPlots();
        }, timeout);
        isStarted = true;
        debugLog('Обновление плотов успешно запущено');
    },
    stop: () => {
        clearInterval(timer);
        isStarted = false;
        debugLog('Обновление плотов успешно остановлено');
    }
};
