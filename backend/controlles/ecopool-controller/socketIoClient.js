const config = require('config');
const IOClient = require('socket.io-client');
const io = IOClient(config.pool.host);
const getLogs = require('../../utils/getLogs');
const { debugLog } = getLogs('ecopool-controller:socket-io-client');

io.on('connect', () => {
    io.emit('setupUser', config.currentUser, payload => {
        const { success } = payload;

        // TODO сделать комментарий более понятливым
        debugLog('setupUser', success);
    });
});

module.exports = io;
