const socketIoClient = require('./socketIoClient');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:make-withdraw');

module.exports = async (payload, password) => {
    let requestData;

    try {
        requestData = await new Promise((resolve) => {
            socketIoClient.emit('create-withdrawal-request', payload, password, resolve);
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Не удалось выполнить запрос на вывод',
            error
        }));
        return { success: false };
    }


    if (!requestData.success) {
        debugError('Не удалось выполнить вывод');
        return requestData;
    }

    debugLog('Вывод выполнен успешно');

    return {
        success: true,
        answer: requestData
    };
};
