const syncSocketRequest = require('./syncSocketRequest');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:pending-withdrawals');

module.exports = async () => {
    let requestData;

    try {
        requestData = syncSocketRequest('pending-withdrawals');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Не удалось выполнить запрос на списание баланса',
            error
        }));
        return { success: false };
    }

    const {
        success,
        answer
    } = requestData;

    if (!success) {
        debugError('Не удалось выполнить списание баланса');
        return requestData;
    }

    debugLog('Списание баланса успешно выполнено');

    return {
        success: true,
        answer
    };
};
