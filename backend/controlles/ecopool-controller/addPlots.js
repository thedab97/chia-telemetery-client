const socketIoClient = require('./socketIoClient');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:add-plots');

module.exports = async (plots) => {
    const chunkSize = 50;
    const plotsLength = plots.length;
    const totalChunks = Math.ceil(plotsLength / chunkSize);
    let currentChunkIdx = 0;

    try {
        for (let i = 0; i < plotsLength; i += chunkSize) {
            const currentChunk = plots.slice(i, i+chunkSize);
            currentChunkIdx++;

            debugLog(`Start sending plots chunk ${currentChunkIdx} of ${totalChunks}`);

            await new Promise(resolve => {
                let resolved = false;
                const resolvePromise = () => {
                    if (!resolved) {
                        resolve();
                    }

                    resolved = true;
                };

                socketIoClient.emit('add-plots', currentChunk, resolvePromise);

                setTimeout(() => {
                    resolvePromise();
                }, 5000);
            });

            debugLog(`End sending plots chunk ${currentChunkIdx} из ${totalChunks}`);
        }
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Error sending data about plots to server',
            error
        }));
    }

    debugLog('Данные по плотам успешно отправлены на сервер');
};
