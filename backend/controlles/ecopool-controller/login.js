const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const setCookie = require('set-cookie-parser');
const GuiController = require('../gui-controller');
const socketIoClient = require('./socketIoClient');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('ecopool-controller:login');
const state = require('../../state');

module.exports = async (payload) => {
    const { login } = payload;
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.login,
            data: payload
        });
    } catch (error) {
        debugError(JSON.stringify({
            'message': 'При логине прозошла ошибка',
            error
        }));
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;
    const parseCookies = setCookie.parse(requestData.headers['set-cookie']);
    const cookieSid = parseCookies.find(cookie => {
        return cookie.name === 'connect.sid';
    });
    const token = cookieSid && cookieSid.value || null;

    if (status !== 200) {
        debugError(`Код ответа сервера: ${status}`);
        return { success: false };
    }

    if (!success) {
        debugLog(JSON.stringify({
            message: 'Не удалось залогиниться',
            reason: requestData.data.answer
        }));
        return {
            success: false,
            answer: requestData.data.answer
        };
    }

    GuiController.localConfig.set('currentUser', login);
    socketIoClient.emit('setupUser', login, ({ success }) => {
        // TODO сделать комментарий более понятливым
        debugLog('Установлен пользователь', success);
    });

    if (token) {
        state.token = token;
    }

    debugLog('Пользователь успешно авторизовался');

    return {
        success: true,
        answer: { token }
    };
};
