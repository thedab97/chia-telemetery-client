const path = require('path');
const fs = require('fs');
const config = require('config');

const setLocalConfig = (key, value) => {
    const pathToLocalConfig = path.join(process.cwd(), './config/local.json');

    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);

        parsedConfig[key] = value;
        fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig));
    } catch (error) {
        fs.writeFileSync(pathToLocalConfig, JSON.stringify({
            [key]: value
        }));
    }

    config[key] = value;

    config.util.loadFileConfigs();

    console.log(config);
};

module.exports = {
    set: setLocalConfig
}
