const path = require('path');
const fs = require('fs');
const config = require('config');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('gui-controller:local-config');

const pathToLocalConfig = path.join(process.cwd(), './config/local.json');
const set = (key, value) => {
    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);

        parsedConfig[key] = value;
        fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig));
        debugLog(`Ключ: ${key} со значением: ${value} в local.json успешно обновлен`);
    } catch (error) {
        fs.writeFileSync(pathToLocalConfig, JSON.stringify({
            [key]: value
        }));
        debugLog(`local.json успешно создался и ключ: ${key} со значением: ${value} успешно добавлен`);
    }

    config[key] = value;
    config.util.loadFileConfigs();
};
const get = (key) => {
    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);
        const hasKey = Boolean(parsedConfig[key]);

        if (hasKey) {
            return parsedConfig[key];
        } else {
            debugError(`Ключ: ${key} не найден в local.json`);
            return null;
        }
    } catch (error) {
        debugError('Не удалось прочитать local.json');
    }
};

module.exports = {
    set,
    get
};
