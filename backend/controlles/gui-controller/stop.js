const localConfig = require('./localConfig');
const state = require('../../state');
const getLogs = require('../../utils/getLogs');
const { debugLog } = getLogs('gui-controller:stop');

module.exports = () => {
    state.token = null;
    localConfig.set('currentUser', null);
    debugLog('Запущенные процессы gui controller-а успешно остановлены');
};
