const localConfig = require('./localConfig');

module.exports = () => {
    return Boolean(localConfig.get('currentUser'));
};
