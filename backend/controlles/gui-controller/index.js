const localConfig = require('./localConfig');
const userIsLogged = require('./userIsLogged');
const createPlot = require('./createPlot');
const createPlots = require('./createPlots');
const stop = require('./stop');

module.exports = {
    localConfig,
    userIsLogged,
    createPlot,
    createPlots,
    stop
};
