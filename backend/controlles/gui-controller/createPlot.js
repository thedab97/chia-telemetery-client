const checkPlottingInQueue = require('../../utils/checkPlottingInQueue');
const createTestPlot = require('../chia-controller/createTestPlot');
const getLogs = require('../../utils/getLogs');
const state = require('../../state');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:create-plot');

const createPlot = async (queue) => {
    if (!queue) {
        debugError('Не передано имя очереди');
        return false;
    }

    const queueData = state.queues[queue];

    if (!queueData) {
        debugError(`Данные по очереди ${queue} не найдены`);
        return false;
    }

    const queueIsPlotting = checkPlottingInQueue(queueData);

    if (queueIsPlotting) {
        debugWarn(`Плоттинг в очереди ${queue} уже запущен`);
        return false;
    }

    const queueLength = queueData.length;
    const plottingPlotData = queueData[queueLength - 1];

    plottingPlotData.starting = true;
    plottingPlotData.process = createTestPlot('/Users/anton/Desktop/chia/', '/Users/anton/Desktop/chia/');

    const plotProcess = plottingPlotData.process;

    debugLog(`Плоттинг в очереди ${queue} успешно запущен`);

    plotProcess.stdout.on('data', (data) => {
        plottingPlotData.logs.push(data.toString().trim());
    });

    plotProcess.on('exit', (exitCode) => {
        if (exitCode === 0) {
            queueData.pop();
            debugLog(`Плоттинг в очереди ${queue} успешно завершён`);

            if (queueLength.length) {
                createPlot(queue);
            } else {
                debugLog(`Очередь ${queue} успешно завершена`);
            }
        } else {
            debugError(`Плоттинг в очереди ${queue} завершился с ошибкой`);
        }
    });
};

module.exports = createPlot;
