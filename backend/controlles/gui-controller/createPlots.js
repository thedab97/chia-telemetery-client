const md5 = require('md5');
const createPlot = require('./createPlot');
const getLogs = require('../../utils/getLogs');
const state = require('../../state');
const { debugLog, debugError } = getLogs('gui-controller:create-plots');

module.exports = (payload = {}) => {
    const {
        size,
        ram,
        threads,
        count,
        buckets,
        bitField,
        queue = 'default',
        tempFolder,
        tempFolder2,
        persistentFolder
    } = payload;

    if (!count) {
        debugError('Не передано количество плотов');
        return false;
    }

    const plotsData = [];
    const { queues } = state;

    for (let i = 0; i < count; i++) {
        plotsData.push({
            id: md5(Date.now() + Math.random()),
            starting: false,
            options: {
                size,
                ram,
                threads,
                buckets,
                bitField,
                tempFolder,
                tempFolder2,
                persistentFolder
            },
            logs: [],
            process: null
        });
    }

    if (!plotsData.length) {
        debugError('Пустой массив с данными плотов');
        return false;
    }

    if (queues[queue] && queues[queue].length) {
        queues[queue] = [
            ...queues[queue],
            ...plotsData
        ]
        debugLog(`В очередь ${queue} успешно добавлены новые плоты`);
    } else {
        queues[queue] = plotsData;
        debugLog(`Очередь ${queue} успешно создана с новыми плотами`);
    }

    createPlot(queue);
};
