const ChiaController = require('../controlles/chia-controller');
const getLogs = require('../utils/getLogs');
const { debugLog } = getLogs('sockets');
let io = null;

module.exports = {
    init: (socketInstance) => {
        debugLog('Sockets successfully connected');
        io = socketInstance;
        ChiaController.logWatcher.start(socketInstance);
    },
    io
};
