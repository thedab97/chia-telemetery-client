const config = require('config');
const debug = require('debug');

module.exports = (logsName) => {
    if (!logsName) {
        return {};
    }

    const debugLog = debug(`${config.debug.name}:log:${logsName}`);
    const debugWarn = debug(`${config.debug.name}:warn:${logsName}`);
    const debugError = debug(`${config.debug.name}:error:${logsName}`);

    debugLog.log = console.log;
    debugWarn.log = console.warn;
    debugError.log = console.error;

    return { debugLog, debugWarn, debugError };
}
