const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/RoutesErrorAnswers');
const GuiController = require('../../controlles/gui-controller');
const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const requestData = await EcoPoolController.pendingWithdrawals();
    const { success, answer } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    return res.send({
        success: true,
        answer
    });
};
