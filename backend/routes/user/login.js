const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');
const ChiaController = require('../../controlles/chia-controller');

router.post('/', async (req, res, next) => {
    const { body: { login, password } } = req;
    const requestData = await EcoPoolController.userLogin({
        login,
        password
    });

    if (!requestData.success) {
        return res.send(requestData);
    }

    const checkChia = await ChiaController.checkCLI();
    const answer = {
        success: true,
        answer: { checkChia }
    }

    if (checkChia) {
        ChiaController.init();
        return res.send(answer);
    } else {
        return res.send(answer);
    }
});

module.exports = router;
