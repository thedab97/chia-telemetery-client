const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/RoutesErrorAnswers');
const GuiController = require('../../controlles/gui-controller');
const EcoPoolController = require('../../controlles/ecopool-controller');

module.exports = async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {
        body: {
            amount,
            xchAddress,
            password,
            timestamp
        }
    } = req;

    if (isNaN(+amount)) {
        return res.send({
            success: false,
            answer: {
                error: 'Amount is not a number!'
            }
        });
    }

    const requestData = await EcoPoolController.makeWithdraw({
        address: xchAddress,
        amount,
        timestamp
    }, password);

    const { success, answer } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    return res.send({
        success: true,
        answer: {
            userBalance: answer
        }
    });
};
