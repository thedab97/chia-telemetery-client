const express = require('express');
const router = express.Router();
const GuiController = require('../../controlles/gui-controller');
const ChiaController = require('../../controlles/chia-controller');

router.get('/', async (req, res) => {
    const answer = {
        success: GuiController.userIsLogged(),
        answer: {
            checkChia: await ChiaController.checkCLI()
        }
    };

    return res.send(answer);
});

module.exports = router;
