const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');

router.get('/', async (req, res) => {
    return res.send(await EcoPoolController.userLogout());
});

module.exports = router;
