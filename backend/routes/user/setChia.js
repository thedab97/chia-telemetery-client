const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/RoutesErrorAnswers');
const GuiController = require('../../controlles/gui-controller');
const ChiaController = require('../../controlles/chia-controller');

module.exports = async (req, res, next) => {
    const { body: { path } } = req;

    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!path) {
        return { success: false };
    }

    const checkCLI = await ChiaController.checkCLI(path);

    if (checkCLI) {
        GuiController.localConfig.set('chiaCliPath', path);
        ChiaController.init();
        return res.send({ success: true });
    } else {
        return res.send({ success: false });
    }
};
