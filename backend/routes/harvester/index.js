const express = require('express');
const router = express.Router();
const farm = require('./farm');
const getCheckPlotsHistory = require('./getCheckPlotsHistory');
const getTelemetryHistory = require('./getTelemetryHistory');

router.get('/farm', farm);
router.get('/get-check-plots-history', getCheckPlotsHistory);
router.get('/get-telemetry-history', getTelemetryHistory);

module.exports = router;
