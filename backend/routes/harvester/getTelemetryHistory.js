const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/RoutesErrorAnswers');
const GuiController = require('../../controlles/gui-controller');
const getLogs = require('../../utils/getLogs');
const { debugLog } = getLogs('routes:harvester:get-telemetry-history');
const state = require('../../state');

module.exports = (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    debugLog('История плотов прошедших фильтр отправлена на клиент');
    return res.send({
        success: true,
        answer: {
            history: state.farmingPlotsHistory
        }
    });
};
