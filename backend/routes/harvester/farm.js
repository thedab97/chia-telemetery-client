const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/RoutesErrorAnswers');
const GuiController = require('../../controlles/gui-controller');
const ChiaController = require('../../controlles/chia-controller');

module.exports = async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const plots = await ChiaController.getPlots(true);
    const badPlots = await ChiaController.getBadPlots();

    return res.send({
        success: true,
        answer: { plots, badPlots }
    });
};
