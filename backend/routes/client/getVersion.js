const axios = require('axios');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('routes:client:get-version');

module.exports = async (req, res) => {
    let requestData;

    try {
        requestData = await axios({
            method: 'get',
            url: 'https://gitlab.com/ecopool-public/chia-telemetery-client/-/raw/master/package.json'
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'При запросе версии из репозитория произошла ошибка',
            error
        }));
        return { success: false };
    }

    debugLog('Версия приложения успешно запрошена');

    res.send({
        success: true,
        answer: {
            version: requestData.data.version
        }
    });
};
