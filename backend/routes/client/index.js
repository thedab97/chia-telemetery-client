const express = require('express');
const router = express.Router();
const getVersion = require('./getVersion');
const update = require('./update');

router.get('/get-version', getVersion);
router.get('/update', update);

module.exports = router;
