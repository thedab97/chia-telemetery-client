const { exec } = require('child_process');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('routes:');

module.exports = async (req, res) => {

    debugLog('Запущен процесс git pull');
    await new Promise(resolve => {
        exec('git pull', {
            cwd: process.cwd()
        }, (error, stdout, stderr) => {
            debugLog(stdout);

            if (stderr) {
                debugError('Не удалось выполнить git pull', stderr);
            }

            resolve();
        });
    });
    debugLog('Процесс git pull завершён');

    debugLog('Запущен процесс yarn install --prune');
    await new Promise(resolve => {
        exec('yarn install --prune', {
            cwd: process.cwd()
        }, (error, stdout, stderr) => {
            debugLog(stdout);

            if (stderr) {
                debugError('Не удалось выполнить yarn install --prune', stderr);
            }

            resolve();
        });
    });
    debugLog('Процесс yarn install --prune завершён');

    debugLog('Запущен процесс ожидания в 10 секунд');
    await new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, 10000);
    });
    debugLog('Процесс ожидания в 10 секунд заверщен');

    res.send();

    debugLog('Запущен процесс pm2 restart all');
    exec('pm2 restart all', {
        detached: true
    }, (error, stdout, stderr) => {
        debugLog(stdout);

        if (stderr) {
            debugError('Не удалось перезапустить сервисы', stderr);
        }
    });
    debugLog('Процесс pm2 restart all завершен');
};
