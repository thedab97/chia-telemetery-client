const express = require('express');
const cookieParser = require('cookie-parser');
const routes = require('./routes');
const app = express();
const ChiaController = require('./controlles/chia-controller');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

routes(app);
ChiaController.init();

module.exports = app;
